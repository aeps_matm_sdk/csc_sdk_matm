package com.matm.matmsdk.matm1;


import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.matm.matmsdk.Utils.SdkConstants;
import com.matm.matmsdk.dmtModule.bluetooth.BluetoothPrinter;
import com.matm.matmsdk.transaction_report.TransactionStatusActivity;

import isumatm.androidsdk.equitas.R;


public class MATMTransactionStatusActivity extends AppCompatActivity {

    LinearLayout successLayout,failureLayout;
    Button okButton,okSuccessButton,printBtn;
    TextView detailsTextView,failureDetailTextView,failureTitleTextView;
    String txnstatus = "NA";
    String balance = "NA";
    String cardnumber = "NA";
    String rferencesnumber = "NA";
    String dataandtime = "NA";
    String amount = "NA";
    String terminal = "NA";
    String transactionType = "NA";

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_matmtransaction_status);

        successLayout = findViewById(R.id.successLayout);
        failureLayout = findViewById(R.id.failureLayout);
        okButton = findViewById(R.id.okButton);
        okSuccessButton = findViewById(R.id.okSuccessButton);
        printBtn = findViewById(R.id.printBtn);
        detailsTextView = findViewById(R.id.detailsTextView);
        failureTitleTextView = findViewById(R.id.failureTitleTextView);
        failureDetailTextView = findViewById(R.id.failureDetailTextView);

        if(getIntent().getSerializableExtra(MatmConstant.MICRO_ATM_TRANSACTION_STATUS_KEY) == null) {
            failureLayout.setVisibility(View.VISIBLE);
            successLayout.setVisibility(View.GONE);
            failureDetailTextView.setText("Some Exception occurred \n Please try again after some time !!!");
        }else{
            MicroAtmTransactionModel microAtmTransactionModel = (MicroAtmTransactionModel) getIntent().getSerializableExtra(MatmConstant.MICRO_ATM_TRANSACTION_STATUS_KEY);

            if (microAtmTransactionModel .getTxnStatus() !=null && microAtmTransactionModel.getTxnAmt() !=null ) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);

                if(microAtmTransactionModel.getTxnStatus() !=null && !microAtmTransactionModel.getTxnStatus().matches("")){
                    txnstatus = microAtmTransactionModel.getTxnStatus();
                }


                if(microAtmTransactionModel.getAvailableBalance() !=null && !microAtmTransactionModel.getAvailableBalance().matches("NA")){
                    balance = microAtmTransactionModel.getAvailableBalance();
                }


                if(microAtmTransactionModel.getCardNumber() != null && !microAtmTransactionModel.getCardNumber().matches("")){
                    cardnumber = microAtmTransactionModel.getCardNumber();
                }

                if(microAtmTransactionModel.getRnr()!= null && !microAtmTransactionModel.getRnr().matches("")){
                    rferencesnumber = microAtmTransactionModel.getRnr();
                }


                if(microAtmTransactionModel.getTransactionDateandTime() !=null && !microAtmTransactionModel.getTransactionDateandTime().matches("")){
                    dataandtime = microAtmTransactionModel.getTransactionDateandTime();
                }


                if(microAtmTransactionModel.getTxnAmt() !=null && !microAtmTransactionModel.getTxnAmt().matches("")){
                    amount = microAtmTransactionModel.getTxnAmt();
                }


                if(microAtmTransactionModel.getTerminalId() !=null && !microAtmTransactionModel.getTerminalId().matches("")){
                    terminal = microAtmTransactionModel.getTerminalId();
                }

                if(microAtmTransactionModel.getType() !=null && !microAtmTransactionModel.getType().matches("")){
                    transactionType = microAtmTransactionModel.getType();
                    detailsTextView.setText ( "Cash Withdrawal for " +cardnumber + " is sucessfully completed . \n \nTransaction Status : "+txnstatus+" \nReference No : "+ rferencesnumber+"\nAvailable Balance : "+ balance+"\nTransaction Amount : "+amount+ " \nTransaction Date and Time : "+dataandtime+" \nTerminal ID : "+terminal);
                }

            }else  if (microAtmTransactionModel.getBalanceEnquiryStatus() != null && microAtmTransactionModel.getAccountNo() != null){
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);


                if(microAtmTransactionModel.getBalanceEnquiryStatus() !=null && !microAtmTransactionModel.getBalanceEnquiryStatus().matches("")){
                    txnstatus = microAtmTransactionModel.getBalanceEnquiryStatus();
                }


                if(microAtmTransactionModel.getAvailableBalance() !=null && !microAtmTransactionModel.getAvailableBalance().matches("NA")){
                    balance = microAtmTransactionModel.getAvailableBalance();
                }


                if(microAtmTransactionModel.getCardNumber() != null && !microAtmTransactionModel.getCardNumber().matches("")){
                    cardnumber = microAtmTransactionModel.getCardNumber();
                }

                if(microAtmTransactionModel.getRnr()!= null && !microAtmTransactionModel.getRnr().matches("")){
                    rferencesnumber = microAtmTransactionModel.getRnr();
                }


                if(microAtmTransactionModel.getTransactionDateandTime() !=null && !microAtmTransactionModel.getTransactionDateandTime().matches("")){
                    dataandtime = microAtmTransactionModel.getTransactionDateandTime();
                }

                String accountNumber = "NA";
                if(microAtmTransactionModel.getAccountNo() !=null && !microAtmTransactionModel.getAccountNo().matches("")){
                    accountNumber = microAtmTransactionModel.getAccountNo();
                }


                if(microAtmTransactionModel.getTerminalId() !=null && !microAtmTransactionModel.getTerminalId().matches("")){
                    terminal = microAtmTransactionModel.getTerminalId();
                }

                if(microAtmTransactionModel.getType() !=null && !microAtmTransactionModel.getType().matches("")){
                    transactionType = microAtmTransactionModel.getType();
                    detailsTextView.setText (txnstatus+" for " +cardnumber + ". \n \nAccount No : "+accountNumber+" \nReference No : "+ rferencesnumber+"\nAvailable Balance : "+ balance+" \nTransaction Date and Time : "+dataandtime+" \nTerminal ID : "+terminal);
                }
            }else if(microAtmTransactionModel.getStatusError() != null && microAtmTransactionModel.getErrormsg() != null &&
                    !microAtmTransactionModel.getStatusError().matches("") && !microAtmTransactionModel.getErrormsg().matches("") &&
                    microAtmTransactionModel.getStatusError().trim().equalsIgnoreCase("success")) {
                failureLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    detailsTextView.setText(microAtmTransactionModel.getErrormsg());
                    detailsTextView.setGravity(Gravity.CENTER);
                }
            }else if(microAtmTransactionModel.getStatusError() != null && microAtmTransactionModel.getErrormsg() != null &&
                    !microAtmTransactionModel.getStatusError().matches("") && !microAtmTransactionModel.getErrormsg().matches("") &&
                    microAtmTransactionModel.getStatusError().trim().equalsIgnoreCase("failed")) {
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    failureDetailTextView.setText(microAtmTransactionModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again !!!");
                }
            }else{
                failureLayout.setVisibility(View.VISIBLE);
                successLayout.setVisibility(View.GONE);
                if(microAtmTransactionModel.getErrormsg() != null && !microAtmTransactionModel.getErrormsg().matches("")){
                    failureDetailTextView .setText (microAtmTransactionModel.getErrormsg());
                }else{
                    failureDetailTextView .setText ("Some Exception occured, Please try again after some time !!!");
                }
            }
        }
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        okSuccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        printBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SdkConstants.BRAND_NAME.trim().length() != 0) {
                    BluetoothDevice bluetoothDevice = SdkConstants.bluetoothDevice;
                    if (bluetoothDevice != null) {
                        callBluetoothFunction(txnstatus, dataandtime, rferencesnumber,terminal,transactionType,balance, cardnumber, amount, bluetoothDevice);
                    } else {
                        finish();
                        Toast.makeText(MATMTransactionStatusActivity.this, "Please connect the printer", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    showBrandSetAlert();
                }
            }
        });
    }



    private void showBrandSetAlert(){
        try{
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MATMTransactionStatusActivity.this);
            builder1.setMessage("Unable to download/print the receipt. Please contact admin.");
            builder1.setTitle("Warning!!!");
            builder1.setCancelable(false);
            builder1.setPositiveButton(
                    "GOT IT",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
        }catch (Exception e){

        }
    }

    private void callBluetoothFunction(final String txnstatus, final String date, final String reffNo,final String terminalId, final String type,final String balance,final String cardNumber, final String transactionAmt,  BluetoothDevice bluetoothDevice) {


        final BluetoothPrinter mPrinter = new BluetoothPrinter(bluetoothDevice);
        mPrinter.connectPrinter(new BluetoothPrinter.PrinterConnectListener() {

            @Override
            public void onConnected() {
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(SdkConstants.SHOP_NAME.toUpperCase());
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_LEFT);
                mPrinter.printText("--------Transaction Report---------");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_CENTER);
                mPrinter.setBold(true);
                mPrinter.printText(txnstatus);
                mPrinter.addNewLine();
                mPrinter.addNewLine();

                mPrinter.printText("Date/Time: "+date);
                mPrinter.addNewLine();
                mPrinter.printText("RefF No.: "+reffNo);
                mPrinter.addNewLine();
                mPrinter.printText("TerminalID: "+terminalId);
                mPrinter.addNewLine();
                mPrinter.printText("TransactionType: "+type);
                mPrinter.addNewLine();
                mPrinter.printText("Balance: "+balance);
                mPrinter.addNewLine();
                mPrinter.printText("CardNumber : "+cardNumber);
                mPrinter.addNewLine();
                mPrinter.printText("TransactionAmount : "+transactionAmt);
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.setBold(true);
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText("Thank You");
                mPrinter.addNewLine();
                mPrinter.setAlign(BluetoothPrinter.ALIGN_RIGHT);
                mPrinter.printText(SdkConstants.BRAND_NAME);
                mPrinter.addNewLine();
                mPrinter.printText("-----------------------------------");
                mPrinter.addNewLine();
                mPrinter.addNewLine();
                mPrinter.finish();
            }

            @Override
            public void onFailed() {
                Log.d("BluetoothPrinter", "Conection failed");
                Toast.makeText(MATMTransactionStatusActivity.this, "Please switch on bluetooth printer", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
